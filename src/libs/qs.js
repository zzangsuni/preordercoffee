import qs from 'qs';

/*
  location/search값 객체형 반환
*/
export default function query(location) {
  return qs.parse(location.search, {
    ignoreQueryPrefix: true, //URL ? 문자 생략
  });
}
