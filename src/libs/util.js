/*
  메일양식 유효성검사
*/
export const checkEmail = address => {
  const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return pattern.test(address);
};

/*
  비밀번호 유효성검사
*/
export const checkPassword = password => {
  const pattern = /[a-z!@#$%^&0-9]{6,}?/;
  return pattern.test(password);
};

/*
  금액단위 설정
*/
export const commas = val => {
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

/*
  배열요소 단일요소 추출
*/
const _each = (list, iter) => {
  let len = list.length;
  for (let i = 0; i < len; i++) {
    iter(list[i]);
  }
};

/*
  객체 배열 전환 (키를 내부속성에 추가)
*/
export const _appendKey = list => {
  const new_list = [];
  let keys = Object.keys(list);
  let len = keys.length;
  for (let i = 0; i < len; i++) {
    const item = list[keys[i]];
    item.key = keys[i];
    new_list.push(item);
  }
  return new_list;
};

/*
  filter정의
*/
export const _filter = (list, predi) => {
  const new_list = [];
  _each(list, function(val) {
    if (predi(val)) {
      new_list.push(val);
    }
  });
  return new_list;
};

/*
  map정의
*/
export const _map = (list, mapper) => {
  const new_list = [];
  _each(list, function(val) {
    new_list.push(mapper(val));
  });
  return new_list;
};
