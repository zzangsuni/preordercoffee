import { useState, useEffect } from 'react';
import axios from '../libs/api';

/*
  HTTP통신 에러반환 사용자 HOOKS
*/
export default () => {
  const [error, setError] = useState(null);
  const response = axios.interceptors.response.use(
    res => res,
    error => {
      setError(error);
    },
  );
  const request = axios.interceptors.request.use(req => {
    setError(null);
    return req;
  });

  useEffect(() => {
    return () => {
      axios.interceptors.request.eject(request);
      axios.interceptors.response.eject(response);
    };
  }, [request, response]);
  return error;
};
