import React from 'react';
import Modal from '../components/modal/Modal';
import httpHandler from './httpHandler';

/*
  에러메시지 처리 모달
*/
const ErroHandler = WithErrorHandler => {
  return props => {
    const error = httpHandler();
    return (
      <>
        <Modal shown={error ? null : true}>
          {error ? error.message : null}
        </Modal>
        <WithErrorHandler {...props} />
      </>
    );
  };
};

export default ErroHandler;
