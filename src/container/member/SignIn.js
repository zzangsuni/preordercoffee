import React from 'react';
import { Contents, PageTitle } from '../../components/common';
import Auth from '../../components/auth/Auth';

const SignIn = () => {
  return (
    <Contents>
      <PageTitle>로그인</PageTitle>
      <div
        style={{
          maxWidth: '500px',
          margin: '0 auto 20px auto',
          textAlign: 'center',
        }}
      >
        테스트계정을 사용하셔도 됩니다.
        <br />
        (test@test.com / 111111)
      </div>
      <Auth type="signin" />
    </Contents>
  );
};

export default SignIn;
