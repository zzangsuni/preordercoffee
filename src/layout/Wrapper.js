import React from 'react';
import styled from 'styled-components';

const WrapStyle = styled.section`
  width: 100%;
  margin: 0 auto;
  padding-top: 60px;
  min-width: 320px;
`;

/*
  전체 페이지 감싸는 LAYOUT
*/
const Wrapper = ({ children }) => {
  return <WrapStyle>{children}</WrapStyle>;
};

export default Wrapper;
