import { combineReducers } from 'redux';
import { all } from 'redux-saga/effects';
import loadings from './loadings';
import error from './error';
import product, { productListAsync } from './product';
import options, { optionsAsync } from './options';
import auth, { authAsync } from './auth';
import store, { storeAsync } from './store';
import wish, { wishAsync } from './wish';
import mymenu, { menuAsync } from './mymenu';
import order, { orderAsync } from './order';

/***********************
  리듀서 병합
************************/
const rootReducers = combineReducers({
  product,
  loadings,
  error,
  options,
  wish,
  auth,
  store,
  mymenu,
  order,
});

/***********************
  비동기 처리 병합
************************/
export function* rootSaga() {
  yield all([
    productListAsync(),
    optionsAsync(),
    authAsync(),
    storeAsync(),
    wishAsync(),
    menuAsync(),
    orderAsync(),
  ]);
}

export default rootReducers;
