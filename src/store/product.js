import { createAction, handleActions } from 'redux-actions';
import { delay, call, put, takeLatest } from 'redux-saga/effects';
import * as api from '../libs/api';
import { startLoading, finishLoading } from './loadings';
import { errorMessage } from './error';
import produce from 'immer';

/*******************
  상품 리듀서
********************/
/* 
  액션타입 
*/
const INIT_PRODUCTS = 'product/INIT_PRODUCTS';
export const SET_PRODUCTS = 'product/SET_PRODUCTS';
const INIT_PRODUCT = 'product/INIT_PRODUCT';
const SET_PRODUCT = 'product/SET_PRODUCT';

/* 
  액션생성함수 
*/
//초기 전체상품 가져오기
export const initProducts = createAction(INIT_PRODUCTS);
//단일상품 해제
export const initProduct = createAction(INIT_PRODUCT);
//단일상품 설정
export const onSetProduct = createAction(SET_PRODUCT, payload => payload);

/* 
  비동기 처리 
*/
//초기 전체상품 가져오기
const fetchListAsync = function*() {
  yield put(startLoading(SET_PRODUCTS));
  try {
    const response = yield call(api.products);
    yield delay(500);
    yield put({
      type: SET_PRODUCTS,
      payload: response.data,
    });
  } catch (e) {
    yield put(errorMessage({ action: SET_PRODUCTS, message: e }));
  }
  yield put(finishLoading(SET_PRODUCTS));
};

//비동기처리 호출
export const productListAsync = function*() {
  yield takeLatest(INIT_PRODUCTS, fetchListAsync);
};

/* 
  초기상태 
*/
const initialState = {
  lists: null,
  product: null,
};

/* 
  리듀서 
*/
const product = handleActions(
  {
    [SET_PRODUCTS]: (state, { payload: products }) =>
      produce(state, draft => {
        draft.lists = products;
      }),
    [INIT_PRODUCT]: (state, action) =>
      produce(state, draft => {
        draft.product = null;
      }),
    [SET_PRODUCT]: (state, { payload: { type, kind } }) =>
      produce(state, draft => {
        let sizes = {};
        const product = state.lists[type][kind];
        const prodSizes = product.sizes;
        if (kind === 'espresso') {
          sizes = {
            Solo: prodSizes['Solo'],
            Doppio: prodSizes['Doppio'],
          };
        } else {
          sizes = {
            Tall: prodSizes['Tall'],
            Grande: prodSizes['Grande'],
            Venti: prodSizes['Venti'],
          };
        }
        product.sizes = sizes;
        draft.product = product;
      }),
  },
  initialState,
);

export default product;
