import { createAction, handleActions } from 'redux-actions';
import { call, put, takeLatest } from 'redux-saga/effects';
import * as api from '../libs/api';
import { startLoading, finishLoading } from './loadings';
import { errorMessage } from './error';
import { produce } from 'immer';

/*******************
  주문 리듀서
********************/
/* 
  액션타입 
*/
export const ORDER_LIST = 'orders/ORDER_LIST';
const INIT_ORDER_LIST = 'orders/INIT_ORDER_LIST';
const SET_ORDER_LIST = 'orders/SET_ORDER_LIST';
export const ADD_ORDER_LIST = 'orders/ADD_ORDER_LIST';
const EMPTY_ORDER_LIST = 'orders/EMPTY_ORDER_LIST';

/* 
  액션생성함수 
*/
//초기 주문내역 가져오기
export const initOrderList = createAction(INIT_ORDER_LIST, payload => payload);
//주문내역 추가
export const addOrderList = createAction(ADD_ORDER_LIST, payload => payload);
//주문내역 상태 비움
export const emptyOrderList = createAction(EMPTY_ORDER_LIST);

/* 
  비동기 처리 
*/
//초기 주문내역 가져오기 [payload : 계정정보, 페이징수, 주문내역수]
const fetchOrderAsync = function*({ payload }) {
  yield put(startLoading(ORDER_LIST));
  try {
    const response = yield call(
      api.getOrder,
      payload.token,
      payload.userId,
      payload.page,
      payload.limit,
    );
    yield put({ type: SET_ORDER_LIST, payload: response.data });
  } catch (e) {
    yield put(errorMessage({ action: ORDER_LIST, message: e }));
  }
  yield put(finishLoading(ORDER_LIST));
};

//주문내역 추가 [payload : 계정정보, 추가할 주문]
const addOrderAsync = function*({ payload }) {
  yield put(startLoading(ADD_ORDER_LIST));
  try {
    yield call(api.addOrder, payload.token, payload.userId, payload.order);
    yield put(finishLoading(ADD_ORDER_LIST));
  } catch (e) {
    yield put(errorMessage({ action: ADD_ORDER_LIST, message: e }));
  }
};

//비동기처리 호출
export const orderAsync = function*() {
  yield takeLatest(INIT_ORDER_LIST, fetchOrderAsync);
  yield takeLatest(ADD_ORDER_LIST, addOrderAsync);
};

/* 
  초기상태 
*/
const initialState = {
  lists: [],
  endAt: null,
  finish: false,
};

/* 
  리듀서
*/
const order = handleActions(
  {
    [SET_ORDER_LIST]: (state, { payload: orders }) =>
      produce(state, draft => {
        let orderList = Object.keys(orders)
          .map((o, index) => orders[o])
          .reverse();
        if (draft.endAt !== null) {
          orderList = orderList.filter((o, index) => index !== 0);
        }
        draft.lists = [...draft.lists, ...orderList];
        draft.endAt = Object.keys(orders)[0];
        draft.finish = Object.keys(orders).length < 7;
      }),
    [EMPTY_ORDER_LIST]: state =>
      produce(state, draft => {
        draft.lists = [];
        draft.endAt = null;
        draft.finish = false;
      }),
  },
  initialState,
);

export default order;
