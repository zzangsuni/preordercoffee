import { createAction, handleActions } from 'redux-actions';
import { call, put, takeLatest } from 'redux-saga/effects';
import { produce } from 'immer';
import * as api from '../libs/api';
import { startLoading, finishLoading } from './loadings';
import { errorMessage } from './error';

/*******************
  매장 리듀서
********************/
/* 
  액션타입 
*/
const INIT_STORE = 'store/INIT_STORE';
export const GET_STORES = 'store/GET_STORES';
const SET_STORE = 'store/SET_STORE';

/* 
  액션생성함수 
*/
//초기 매장정보 가져오기
export const initStore = createAction(INIT_STORE);
//픽업 매장선택
export const setStore = createAction(SET_STORE, store => store);

/* 
  비동기 처리 
*/
//초기 매장정보 가져오기
const fetchStoreAsync = function*() {
  yield put(startLoading(GET_STORES));
  try {
    const response = yield call(api.store);
    yield put({
      type: GET_STORES,
      payload: response.data,
    });
  } catch (e) {
    yield put(errorMessage({ action: GET_STORES, message: e }));
  }
  yield put(finishLoading(GET_STORES));
};

//비동기처리 호출
export const storeAsync = function*() {
  yield takeLatest(INIT_STORE, fetchStoreAsync);
};

/* 
  초기상태 
*/
const initialState = {
  stores: null,
  selected: null,
};

/* 
  리듀서 
*/
const store = handleActions(
  {
    [GET_STORES]: (state, { payload: stores }) =>
      produce(state, draft => {
        draft.stores = stores;
      }),
    [SET_STORE]: (state, { payload: store }) =>
      produce(state, draft => {
        draft.selected = store;
      }),
  },
  initialState,
);

export default store;
