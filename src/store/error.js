import { createAction, handleActions } from 'redux-actions';
import produce from 'immer';

/***********************
  에러 처리 리듀서
************************/
/* 
  액션타입 
*/
const ERROR_MESSAGE = 'error/ERROR_MESSAGE';
const EMPTY_MESSAGE = 'error/EMPTY_MESSAGE';

/* 
  액션생성함수 
*/
//에러메시지 저장 [payload : 에러객체 받음]
export const errorMessage = createAction(ERROR_MESSAGE, payload => payload);
//에러메시지 삭제
export const emptyMessage = createAction(EMPTY_MESSAGE, type => type);

/* 
  초기상태 
*/
const initialState = {};

/* 
  리듀서
*/
const error = handleActions(
  {
    [ERROR_MESSAGE]: (state, { payload }) =>
      produce(state, draft => {
        draft[payload.action] = payload.error;
      }),
    [EMPTY_MESSAGE]: (state, { payload: type }) =>
      produce(state, draft => {
        draft[type] = null;
      }),
  },
  initialState,
);

export default error;
