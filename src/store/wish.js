import { createAction, handleActions } from 'redux-actions';
import { call, put, takeLatest, takeEvery } from 'redux-saga/effects';
import * as api from '../libs/api';
import { startLoading, finishLoading } from './loadings';
import { errorMessage } from './error';

/*******************
  위시리스트 리듀서
********************/
/* 
  액션타입 
*/
export const WISH_LIST = 'wish/WISH_LIST';
const INIT_WISH_LIST = 'wish/INIT_WISH_LIST';
const EMPTY_WISH_LIST = 'wish/EMPTY_WISH_LIST';
const SET_WISH_LIST = 'wish/SET_WISH_LIST';
const UPDATE_WISH_LIST = 'wish/UPDATE_WISH_LIST';
const CHECKED_WISH = 'wish/CHECKED_WISH';
export const ADD_WISH = 'wish/ADD_WISH';
export const REMOVE_WISH = 'wish/REMOVE_WISH';

/* 
  액션생성함수
*/
//초기 위시리스트 가져옴
export const initWishList = createAction(INIT_WISH_LIST, auth => auth);
//로그아웃시 위시리스트 상태 비움
export const emptyWishList = createAction(EMPTY_WISH_LIST);
//전체 위시상품 토글
export const updateWishList = createAction(
  UPDATE_WISH_LIST,
  payload => payload,
);
//단일 위시상품 토글
export const checkedWish = createAction(CHECKED_WISH, payload => payload);
//위시상품 추가
export const addWish = createAction(ADD_WISH, wish => wish);
//위시상품 삭제
export const removeWish = createAction(REMOVE_WISH, payload => payload);

/* 
  비동기처리 
*/
//초기 위시리스트 [auth : 계정정보]
const fetchWishAsync = function*({ payload: auth }) {
  yield put(startLoading(WISH_LIST));

  try {
    const response = yield call(api.getWish, auth.token, auth.userId);
    yield put({ type: SET_WISH_LIST, payload: response.data });
  } catch (e) {
    yield put(errorMessage({ action: WISH_LIST, message: e }));
  }
  yield put(finishLoading(WISH_LIST));
};

//위시상품 추가 [payload : 계정정보, 추가할 상품]
const addWishAsync = function*({ payload }) {
  yield put(startLoading(ADD_WISH));
  try {
    yield call(api.addWish, payload.token, payload.userId, payload.wish);
    yield put({
      type: INIT_WISH_LIST,
      payload: {
        token: payload.token,
        userId: payload.userId,
      },
    });
  } catch (e) {
    yield put(errorMessage({ action: ADD_WISH, message: e }));
  }
  yield put(finishLoading(ADD_WISH));
};

//전체 위시상품 토글 [payload : 계정정보, 다중상품]
const updateWishAsync = function*({ payload }) {
  try {
    yield call(api.updateWish, payload.token, payload.userId, payload.wish);
    yield put({
      type: INIT_WISH_LIST,
      payload: {
        token: payload.token,
        userId: payload.userId,
      },
    });
  } catch (e) {
    yield put(errorMessage({ action: UPDATE_WISH_LIST, message: e }));
  }
};

//단일 위시상품 토글 [payload : 계정정보, 단일상품]
const updateAWishAsync = function*({ payload }) {
  try {
    yield call(
      api.updateAWish,
      payload.token,
      payload.userId,
      payload.id,
      payload.wish,
    );
    yield put({
      type: INIT_WISH_LIST,
      payload: {
        token: payload.token,
        userId: payload.userId,
      },
    });
  } catch (e) {
    yield put(errorMessage({ action: CHECKED_WISH, message: e }));
  }
};

//위시상품 삭제 [payload : 계정정보, 삭제상품 id]
const removeWishAsync = function*({ payload }) {
  yield put(startLoading(REMOVE_WISH));
  try {
    yield call(api.removeWish, payload.token, payload.userId, payload.id);
    yield put({
      type: INIT_WISH_LIST,
      payload: {
        token: payload.token,
        userId: payload.userId,
      },
    });
  } catch (e) {
    yield put(errorMessage({ action: REMOVE_WISH, message: e }));
  }
  yield put(finishLoading(REMOVE_WISH));
};

//비동기처리 호출
export const wishAsync = function*() {
  yield takeLatest(INIT_WISH_LIST, fetchWishAsync);
  yield takeEvery(ADD_WISH, addWishAsync);
  yield takeEvery(REMOVE_WISH, removeWishAsync);
  yield takeEvery(CHECKED_WISH, updateAWishAsync);
  yield takeEvery(UPDATE_WISH_LIST, updateWishAsync);
};

/* 
  초기상태 
*/
const initialState = {};

/* 
  리듀서 
*/
const wish = handleActions(
  {
    [SET_WISH_LIST]: (state, { payload: lists }) => lists,
    [EMPTY_WISH_LIST]: state => initialState,
  },
  initialState,
);

export default wish;
