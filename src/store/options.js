import { createAction, handleActions } from 'redux-actions';
import { call, put, takeLatest } from 'redux-saga/effects';
import produce from 'immer';
import * as api from '../libs/api';
import { startLoading, finishLoading } from './loadings';
import { errorMessage } from './error';

/*******************
  퍼스널 옵션 리듀서
********************/
/* 
  액션타입 
*/
const INIT_OPTIONS = 'options/INIT_OPTIONS';
export const SET_OPTIONS = 'options/SET_OPTIONS';
const SET_PRICE = 'options/SET_PRICE';
const SET_ICE = 'options/SET_ICE';
const SET_WATER = 'options/SET_WATER';
const SET_MILK = 'options/SET_MILK';
const SET_SHOT = 'options/SET_SHOT';
const SET_SYRUP = 'options/SET_SYRUP';
const SET_WHIPPING = 'options/SET_WHIPPING';
const SET_COUNT = 'options/SET_COUNT';
const SET_SIZE = 'options/SET_SIZE';
const SET_CUP = 'options/SET_CUP';
const SET_TYPE = 'options/SET_TYPE';
const SET_TOTAL = 'options/SET_TOTAL';
const SET_MESSAGES = 'options/SET_MESSAGES';

/* 
  액션생성함수 
*/
//초기 옵션 가져오기
export const initOptions = createAction(INIT_OPTIONS);
//선택한 상품 단가 설정
export const onSetPrice = createAction(SET_PRICE, price => price);
//얼음 설정
export const onSetIce = createAction(SET_ICE, ice => ice);
//물 설정
export const onSetWater = createAction(SET_WATER, water => water);
//우유 설정
export const onSetMilk = createAction(SET_MILK, milk => milk);
//에스프레소샷 설정
export const onSetShot = createAction(SET_SHOT, shot => shot);
//시럽 설정
export const onSetSyrup = createAction(SET_SYRUP, syrup => syrup);
//휘핑 설정
export const onSetWhipping = createAction(SET_WHIPPING, whipping => whipping);
//음료 갯수 설정
export const onSetCount = createAction(SET_COUNT, count => count);
//음료 사이즈 설정
export const onSetSize = createAction(SET_SIZE, size => size);
//컵 설정
export const onSetCup = createAction(SET_CUP, cup => cup);
//타입(핫/아이스) 설정
export const onSetType = createAction(SET_TYPE, type => type);
//전체 금액 설정
export const onSetTotal = createAction(SET_TOTAL, total => total);
//옵션 국문 설정
export const onSetMessages = createAction(SET_MESSAGES, messages => messages);

/* 
  비동기 처리 
*/
//초기 옵션 가져오기
function* fetchOPtionAsync() {
  yield put(startLoading(SET_OPTIONS));
  try {
    const response = yield call(api.options);
    yield put({
      type: SET_OPTIONS,
      payload: response.data,
    });
  } catch (e) {
    yield put(errorMessage({ action: SET_OPTIONS, message: e }));
  }
  yield put(finishLoading(SET_OPTIONS));
}

//비동기처리 호출
export function* optionsAsync() {
  yield takeLatest(INIT_OPTIONS, fetchOPtionAsync);
}

/* 
  퍼스널 옵션 [국문] 
*/
//타입
export const types = {
  hot: 'HOT',
  iced: 'ICED',
};
//컵
export const cups = {
  mug: '머그컵',
  disposable: '일회용',
  individual: '개인컵',
};
//시럽
export const syrup = {
  mocha: '모카 시럽',
  hazelnut: '헤이즐넛 시럽',
  caramel: '카라멜 시럽',
  vanilla: '바닐라 시럽',
};
//디카페인
export const decaffeine = {
  none: '디카페인',
  half: '1/2 디카페인',
};
//물
export const water = {
  small: '적게',
  normal: '보통',
  large: '많이',
};
//얼굴
export const ice = {
  small: '적게',
  normal: '보통',
  large: '많이',
};
//우유
export const milk = {
  kind: {
    milk: '일반 우유',
    free: '무지방',
    row: '저지방',
    soy: '두유',
  },
  volume: {
    small: '적게',
    normal: '보통',
    large: '많이',
  },
};
//휘핑
export const whipping = {
  small: '적게',
  normal: '보통',
  large: '많이',
};

/* 
  초기상태 
*/
const initialState = {};

/* 
  리듀서
*/
const options = handleActions(
  {
    [SET_OPTIONS]: (state, { payload: data }) => data,
    [SET_PRICE]: (state, { payload: price }) =>
      produce(state, draft => {
        draft.price = price;
      }),
    [SET_ICE]: (state, { payload: ice }) =>
      produce(state, draft => {
        draft.ice = ice;
      }),
    [SET_WATER]: (state, { payload: water }) =>
      produce(state, draft => {
        draft.water = water;
      }),
    [SET_MILK]: (state, { payload: milk }) =>
      produce(state, draft => {
        draft.milk = milk;
      }),
    [SET_SHOT]: (state, { payload: shot }) =>
      produce(state, draft => {
        draft.shot = shot;
      }),
    [SET_SYRUP]: (state, { payload: syrup }) =>
      produce(state, draft => {
        draft.syrup = syrup;
      }),
    [SET_WHIPPING]: (state, { payload: whipping }) =>
      produce(state, draft => {
        draft.whipping = whipping;
      }),
    [SET_COUNT]: (state, { payload: count }) =>
      produce(state, draft => {
        draft.count = count;
      }),
    [SET_CUP]: (state, { payload: cup }) =>
      produce(state, draft => {
        draft.cup = cup;
      }),
    [SET_SIZE]: (state, { payload: size }) =>
      produce(state, draft => {
        draft.size = size;
      }),
    [SET_TYPE]: (state, { payload: type }) =>
      produce(state, draft => {
        draft.type = type;
      }),
    [SET_TOTAL]: (state, { payload: total }) =>
      produce(state, draft => {
        draft.total = total;
      }),
    [SET_MESSAGES]: (state, { payload: messages }) =>
      produce(state, draft => {
        Object.keys(messages).forEach(message => {
          draft.messages[message] = messages[message];
        });
      }),
  },
  initialState,
);

export default options;
