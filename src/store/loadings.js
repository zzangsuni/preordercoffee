import { createAction, handleActions } from 'redux-actions';
import { produce } from 'immer';

/***********************
  비동기 상태처리 리듀서
************************/
/* 
  액션타입 
*/
const START_LOADING = 'loadings/START_LOADING';
const FINISH_LOADING = 'loadings/FINISH_LOADING';
const EMPTY_LOADING = 'loadings/EMPTY_LOADING';

/* 
  액션생성함수 
*/
//진행중
export const startLoading = createAction(
  START_LOADING,
  requestType => requestType,
);
//진행완료
export const finishLoading = createAction(
  FINISH_LOADING,
  requestType => requestType,
);
//상태비움
export const emptyLoading = createAction(EMPTY_LOADING, type => type);

/* 
  초기상태 
*/
const initialState = {};

/* 
  리듀서
*/
const loadings = handleActions(
  {
    [START_LOADING]: (state, action) => ({ ...state, [action.payload]: true }),
    [FINISH_LOADING]: (state, action) => ({
      ...state,
      [action.payload]: false,
    }),
    [EMPTY_LOADING]: (state, { payload: type }) =>
      produce(state, draft => {
        draft[type] = null;
      }),
  },
  initialState,
);

export default loadings;
