import { createAction, handleActions } from 'redux-actions';
import { call, put, takeLatest } from 'redux-saga/effects';
import produce from 'immer';
import * as api from '../libs/api';
import { startLoading, finishLoading } from './loadings';
import { errorMessage } from './error';

/*******************
  나만의 메뉴 리듀서
********************/
/* 
  액션타입 
*/
const INIT_AUTH = 'auth/INIT_AUTH';
export const SET_AUTH = 'auth/SET_AUTH';
const REMOVE_AUTH = 'auth/REMOVE_AUTH';
const ACCESS_AUTH = 'auth/ACCESS_AUTH';

/* 
  액션생성함수 
*/
//회원계정 가져오기
export const initAuth = createAction(INIT_AUTH, userInfo => userInfo);
//회원계정 상태비움
export const removeAuth = createAction(REMOVE_AUTH, success => success);
//세션 유지
export const accessAuth = createAction(ACCESS_AUTH);

/* 
  비동기 처리 
*/
//회원계정 가져오기
const fetchAuthAsync = function*({ payload: userInfo }) {
  yield put(startLoading(SET_AUTH));
  try {
    const response = yield call(api.auth, userInfo);
    yield put({
      type: SET_AUTH,
      payload: { data: response.data, status: response.status },
    });
  } catch (e) {
    yield put(errorMessage({ action: SET_AUTH, error: e.response.data.error }));
  }
  yield put(finishLoading(SET_AUTH));
};

//비동기처리 호출
export const authAsync = function*() {
  yield takeLatest(INIT_AUTH, fetchAuthAsync);
};

/* 
  초기상태 
*/
const initialState = {
  idToken: null,
  localId: null,
  expiresDate: null,
  success: null,
  longTime: false,
};

/* 
  리듀서
*/
const auth = handleActions(
  {
    [SET_AUTH]: (state, { payload }) =>
      produce(state, draft => {
        const expiresDate = new Date(new Date().getTime() + 1800000); //10분 토큰시간
        const data = payload.data;
        localStorage.setItem('idToken', data.idToken);
        localStorage.setItem('localId', data.localId);
        localStorage.setItem('expiresDate', expiresDate);
        draft.idToken = data.idToken;
        draft.localId = data.localId;
        draft.expiresDate = expiresDate;
        draft.success = payload.status >= 200 && payload.status < 300;
      }),
    [REMOVE_AUTH]: (state, { payload: success }) =>
      produce(state, draft => {
        draft.idToken = null;
        draft.localId = null;
        draft.expiresDate = null;
        draft.success = success;
        localStorage.removeItem('idToken');
        localStorage.removeItem('localId');
        localStorage.removeItem('expiresDate');
      }),
    [ACCESS_AUTH]: (state, { payload: expiresIn }) =>
      produce(state, draft => {
        const expiresDate = new Date(new Date().getTime() + expiresIn);
        draft.idToken = localStorage.getItem('idToken');
        draft.localId = localStorage.getItem('localId');
        draft.expiresDate = expiresDate;
        localStorage.setItem('expiresDate', expiresDate);
      }),
  },
  initialState,
);

export default auth;
