import React, { useRef, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ModalContents } from '../common';
import Backdrop from '../backdrop/Backdrop';

/* 
  STYLE
*/
const ModalWrap = styled.div`
  position: fixed;
  z-index: 10;
`;

const Modal = ({ children, shown, ariaLive, onClickHandler }) => {
  const modalRef = useRef(null); //알림모달 요소

  //알림모달 가림
  const onChangePageHandler = useCallback(() => {
    modalRef.current.hidden = true;
  }, []);

  //모달 닫힘 처리
  const onClickParentHandler = () => {
    onClickHandler();
    onChangePageHandler();
  };
  return (
    <>
      <ModalWrap
        hidden={shown}
        ref={modalRef}
        role="alert"
        aria-live="assertive"
      >
        <Backdrop
          onClick={onClickHandler ? onClickParentHandler : onChangePageHandler}
        />
        <ModalContents>{children}</ModalContents>
      </ModalWrap>
    </>
  );
};

Modal.propTypes = {
  children: PropTypes.node,
  shown: PropTypes.bool,
  onClickHandler: PropTypes.func,
};

export default React.memo(Modal);
